import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { WatchComponent } from "./app/pages/watch/watch.component";
import { HisoryComponent } from "./app/pages/hisory/hisory.component";
import { BookmarksComponent } from "./app/pages/bookmarks/bookmarks.component";

const routes: Routes = [
  { path: "watch", component: WatchComponent },
  { path: "", redirectTo: "/watch", pathMatch: "full" },
  { path: "history", component: HisoryComponent },
  { path: "bookmarks", component: BookmarksComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
