import { Component, OnInit, Input, SimpleChanges } from "@angular/core";

@Component({
  selector: "app-video-component",
  templateUrl: "./video-component.component.html",
  styleUrls: ["./video-component.component.css"]
})
export class VideoComponent implements OnInit {
  @Input() url: String;
  id: String;
  constructor() {}

  ngOnInit(): void {
    const tag = document.createElement("script");
    tag.src = "https://www.youtube.com/iframe_api";
    document.body.appendChild(tag);
    this.id = this.url.split("=")[1];
  }
  ngOnChanges(changes: SimpleChanges) {
    this.ngOnInit();
  }
}
