import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { Bookmarks } from "../models/bookmarks";
@Injectable({
  providedIn: "root"
})
export class BookmarksService {
  private baseUrl = "http://127.0.0.1:8000/bookmarks/"; // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ "Content-Type": "application/json" })
  };

  constructor(private http: HttpClient) {}

  /** GET heroes from the server */
  getBookmarks(): Observable<Bookmarks[]> {
    return this.http.get<Bookmarks[]>(this.baseUrl).pipe(
      tap(_ => console.log("fetched bookmarks")),
      catchError(this.handleError<Bookmarks[]>("getBookmarks", []))
    );
  }

  addVideo(video: Bookmarks): Observable<Bookmarks> {
    return this.http
      .post<Bookmarks>(this.baseUrl, video, this.httpOptions)
      .pipe(
        tap((newVideo: Bookmarks) =>
          //  this.log(`added video to bookmarks w/ id=${newVideo.id}`)
          console.log("video added")
        ),
        catchError(this.handleError<Bookmarks>("addVideo"))
      );
  }
  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      //   this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
