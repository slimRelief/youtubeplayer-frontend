import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { HistoryService } from "src/app/services/history.service";
import { History } from "../../models/history";
import { BookmarksService } from "src/app/services/bookmarks.service";
import { Bookmarks } from "src/app/models/bookmarks";

@Component({
  selector: "app-watch",
  templateUrl: "./watch.component.html",
  styleUrls: ["./watch.component.css"]
})
export class WatchComponent implements OnInit {
  url: String;
  clicked: boolean = false;
  okToRefresh: boolean = true;
  refresh: boolean = false;
  oldUrl: String;

  constructor(
    private historyService: HistoryService,
    private bookmarksService: BookmarksService
  ) {}

  ngOnInit(): void {}

  onClick(url: String): void {
    this.url = url;
    this.oldUrl = url;
    this.clicked = true;
    if (this.okToRefresh) {
      /* url = url.trim();
      if (!url) {
        return;
      }*/
      this.historyService.addVideo({ url } as History).subscribe(history => {
        console.log("done adding vid to history");
      });
      this.okToRefresh = false;
    } else {
      this.okToRefresh = true;
    }
  }
  addToBookmarks(url: String): void {
    this.bookmarksService
      .addVideo({ url } as Bookmarks)
      .subscribe(bookmarks => {
        console.log("done adding a favourite vid");
      });
  }
}
