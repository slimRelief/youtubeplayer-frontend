import { Component, OnInit } from "@angular/core";
import { BookmarksService } from "src/app/services/bookmarks.service";
import { Bookmarks } from "src/app/models/bookmarks";

@Component({
  selector: "app-bookmarks",
  templateUrl: "./bookmarks.component.html",
  styleUrls: ["./bookmarks.component.css"]
})
export class BookmarksComponent implements OnInit {
  videos: Bookmarks[];
  constructor(private bookmarksService: BookmarksService) {}

  ngOnInit(): void {
    this.getBookmarks();
  }

  getBookmarks(): void {
    this.bookmarksService.getBookmarks().subscribe(bookmarks => {
      this.videos = bookmarks;
    });
  }
}
