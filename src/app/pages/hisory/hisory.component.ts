import { Component, OnInit } from "@angular/core";
import { HistoryService } from "src/app/services/history.service";
import { History } from "../../models/history";
@Component({
  selector: "app-hisory",
  templateUrl: "./hisory.component.html",
  styleUrls: ["./hisory.component.css"]
})
export class HisoryComponent implements OnInit {
  videos: History[];
  constructor(private historyService: HistoryService) {}

  ngOnInit(): void {
    this.getHistory();
  }

  getHistory(): void {
    this.historyService.getHistory().subscribe(history => {
      this.videos = history;
    });
  }
}
