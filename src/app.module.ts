import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { VideoModule } from "./app/components/video.module";
import { WatchComponent } from "./app/pages/watch/watch.component";
import { HisoryComponent } from "./app/pages/hisory/hisory.component";
import { BookmarksComponent } from "./app/pages/bookmarks/bookmarks.component";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    WatchComponent,
    HisoryComponent,
    BookmarksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    VideoModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
